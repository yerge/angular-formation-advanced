import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { platformBrowserDynamicTesting, BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { User } from '../models/user.interface';

const data :User[] = [

    {
      id: 0,
      "name": "Philipps",
      "surname": "Kevin",
      "age": 24,
      "profession": "Military",
      "address": {
          "roadNumber": 8,
          "street": "Route de la Narbonne",
          "postalCode": 1000,
          "city": "Geneva",
          "country": "Switzerland"
        },
        "mail": "kevin@test.uk",
        "phone": "079 123 45 67"
    },
    {
      "id": 1,
      "name": "Carla",
      "surname": "Carlita",
      "age": 35,
      "profession": "Politian",
      "address": {
          "roadNumber": 10,
          "street": "Route de Versoix",
          "postalCode": 2000,
          "city": "Lausanne",
          "country": "Switzerland"
        },
        "mail": "carlita@test.ch",
        "phone": "079 123 45 68"
    },
    {
      "id": 2,
      "name": "Raul",
      "surname": "Garcia",
      "age": 25,
      "profession": "Player",
      "address": {
          "roadNumber": 11,
          "street": "Route de la catalogne",
          "postalCode": 3000,
          "city": "Barcelona",
          "country": "Spain"
        },
        "mail": "raul@test.cat",
        "phone": "079 123 45 69"
    },
    {
      "id": 3,
      "name": "Raj",
      "surname": "Malhotra",
      "age": 55,
      "profession": "Actor",
      "address": {
          "roadNumber": 12,
          "street": "Route de l'Inde",
          "postalCode": 4000,
          "city": "Amristar",
          "country": "India"
        },
        "mail": "raj@test.in",
        "phone": "079 123 45 70"
    },
    {
      "id": 4,
      "name": "Frank",
      "surname": "LeBoeuf",
      "age": 45,
      "profession": "Player",
      "address": {
          "roadNumber": 13,
          "street": "Route de la France",
          "postalCode": 5000,
          "city": "Paris",
          "country": "France"
        },
        "mail": "frank@test.fr",
        "phone": "079 123 45 71"
    },
    {
      "id": 5,
      "name": "Messi",
      "surname": "Leo",
      "age": 32,
      "profession": "Player",
      "address": {
          "roadNumber": 14,
          "street": "Route de la Catalogne",
          "postalCode": 6000,
          "city": "Barcelona",
          "country": "Spain"
        },
        "mail": "leo@test.cat",
        "phone": "079 123 45 72"
    },
    {
      "id": 6,
      "name": "Cristiano",
      "surname": "Ronaldo",
      "age": 34,
      "profession": "Player",
      "address": {
          "roadNumber": 15,
          "street": "Route de la vieille dame",
          "postalCode": 7000,
          "city": "Turin",
          "country": "Italia"
        },
        "mail": "ronaldo@test.it",
        "phone": "079 123 45 73"
    },
    {
      "id": 7,
      "name": "Prem",
      "surname": "Khurrana",
      "age": 54,
      "profession": "Actor",
      "address": {
          "roadNumber": 16,
          "street": "Route du Mumbai",
          "postalCode": 8000,
          "city": "Mumbai",
          "country": "India"
        },
        "mail": "prem@test.in",
        "phone": "079 123 45 74"
    },
    {
      "id": 8,
      "name": "Chopra",
      "surname": "Yash",
      "age": 84,
      "profession": "Director/Producer",
      "address": {
          "roadNumber": 17,
          "street": "Route de Amristar",
          "postalCode": 9000,
          "city": "Amristar",
          "country": "India"
        },
        "mail": "yash@test.in",
        "phone": "079 123 45 75"
    },
    {
      "id": 9,
      "name": "Trump",
      "surname": "Donald",
      "age": 74,
      "profession": "Politian/Businessman",
      "address": {
          "roadNumber": 18,
          "street": "Route de New York",
          "postalCode": 10000,
          "city": "Washington",
          "country": "USA"
        },
        "mail": "donald@test.com",
        "phone": "079 123 45 76"
    },
    {
      "id": 10,
      "name": "Macron",
      "surname": "Emmanuel",
      "age": 42,
      "profession": "Politian",
      "address": {
          "roadNumber": 19,
          "street": "Route de l'Elysée",
          "postalCode": 10000,
          "city": "Paris",
          "country": "France"
        },
        "mail": "emmanuel@test.fr",
        "phone": "079 123 45 77"
    }
  ];

  const userDetails = {
    data: {
      "id": 10,
      "name": "Macron",
      "surname": "Emmanuel",
      "age": 42,
      "profession": "Politian",
      "address": {
          "roadNumber": 19,
          "street": "Route de l'Elysée",
          "postalCode": 10000,
          "city": "Paris",
          "country": "France"
        },
        "mail": "emmanuel@test.fr",
        "phone": "079 123 45 77"
    }
  };

  const userDetailsObject = {
    "id": 10,
    "name": "Macron",
    "surname": "Emmanuel",
    "age": 42,
    "profession": "Politian",
    "address": {
        "roadNumber": 19,
        "street": "Route de l'Elysée",
        "postalCode": 10000,
        "city": "Paris",
        "country": "France"
      },
      "mail": "emmanuel@test.fr",
      "phone": "079 123 45 77"
  };

  const userDeleted = {
    "id": 9,
    "name": "Trump",
    "surname": "Donald",
    "age": 74,
    "profession": "Politian/Businessman",
    "address": {
        "roadNumber": 18,
        "street": "Route de New York",
        "postalCode": 10000,
        "city": "Washington",
        "country": "USA"
      },
      "mail": "donald@test.com",
      "phone": "079 123 45 76"
  };

  const userToCreate = {
    "id": 11,
    "name": "KHAN",
    "surname": "Shah Rukh",
    "age": 55,
    "profession": "Actor/Producer",
    "address": {
        "roadNumber": 19,
        "street": "Route de New Delhi",
        "postalCode": 31000,
        "city": "New Delhi",
        "country": "India"
      },
      "mail": "shahrukh@india.in",
      "phone": "079 123 45 75"
  };

  const userUpdated = {
    "id": 8,
    "name": "Chopra",
    "surname": "Yash",
    "age": 84,
    "profession": "Director/Producer",
    "address": {
        "roadNumber": 17,
        "street": "Route de Amristar",
        "postalCode": 9000,
        "city": "Amristar",
        "country": "India"
      },
      "mail": "yash@test.in",
      "phone": "079 123 45 75"
  };

  TestBed.initTestEnvironment(
    BrowserDynamicTestingModule,
    platformBrowserDynamicTesting()
  );
  
  describe('UserService', () => {
    //let service: UserService;
    let service: UserService;
    let httpMock: HttpTestingController;
  
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [UserService]
      });
      service = TestBed.inject(UserService);
      httpMock = TestBed.inject(HttpTestingController);
    });
  
    afterEach(() => {
      //httpMock.verify();
    });
  
    it('getUsers() should return data', () => {
      //expect(10).toBe(10);
      service.getUsers().subscribe((res) => {
        expect(res).toEqual(data);
        expect(res.length).toBe(11);
      });

      //const req = httpMock.expectOne('http://localhost/api/users');
      //expect(req.request.method).toBe('GET');
  
    });

   /*  it('getUserById() should return the user details', () => {
      service.getUserById(10).subscribe((res) => {
        expect(JSON.stringify(res)).toEqual(JSON.stringify(userDetails));
        let userModel: User = new User(userDetailsObject);
        expect(res).toEqual(userModel);
      });
    });

    it('deleteUser() should remove the user from the list', () => {
      service.deleteUser(9).subscribe((res) => {
        let lDeletedUser: User = new User(userDeleted);
        expect(res).toEqual(lDeletedUser);
      });
    }); */

    // test update and create
/*     it('createUser() should create a new user', () => {
      let lUserCreated: User = new User(userToCreate);
      data.push(lUserCreated);

      service.createUser(lUserCreated).subscribe(
        () => {
          lData = service;
        }
      );

    
    }); */


  });

/* describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('#getUsers() should return data', () => {
    service.getUsers().subscribe((res) => {
      //expect(res.length).toEqual(data);
      expect(res.length).toBe(11);
    });

  });
}); */
