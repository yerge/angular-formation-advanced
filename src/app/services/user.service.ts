import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../models/user.interface';
import { ObjectMapper } from '../../../node_modules/json-object-mapper';

const USER_API = "api/users";

@Injectable()
export class UserService {

  private _onUserUpdated: Subject<User> = new Subject();
  private _onUserDeleted: Subject<User> = new Subject();
  private _onUserCreated: Subject<User> = new Subject();

  constructor(private http: HttpClient) { }

  public onUserUpdated$(): Observable<User> {
    return this._onUserUpdated.asObservable();
  }

  public onUserDeleted$(): Observable<User> {
    return this._onUserDeleted.asObservable();
  }

  public onUserCreated$(): Observable<User> {
    return this._onUserCreated.asObservable();
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(USER_API)
      .pipe(
        map((data: User[]) => {
          let result = ObjectMapper.deserializeArray<User>(User, data);
          return result;
        })
        //catchError(this.handleError)
      );
  }

  getUserById(id: Number): Observable<User> {

    return this.http.get<User>(`${USER_API}/${id}`)
      .pipe(
        map((data: User) => ObjectMapper.deserialize<User>(User, data))
        //catchError(this.handleError)
      );
  }

  deleteUser(id: Number): Observable<User> {
    let userToDelete: User;
    this.getUserById(id)
          .subscribe((data: User) => {
            userToDelete = data;
    });

    return this.http
      .delete<User>(`${USER_API}/${id}`)
      .pipe(
        tap(() => this._onUserDeleted.next(userToDelete))
        //catchError(this.handleError)
      );
  }

  updateUser(id: number, user: User): Observable<User> {
    const currentUser: User = user;
    return this.http.put<User>(`${USER_API}/${id}`, user)
      .pipe(
        //map((data) => ObjectMapper.serialize(data)),
        tap(() => this._onUserUpdated.next(currentUser))
        //catchError(this.handleError)
      );
  }

  createUser(user: User): Observable<User> {
    const currentUser: User = user;
    return this.http.post<User>(USER_API, user)
      .pipe(
        tap(() => this._onUserCreated.next(currentUser))
        //catchError(this.handleError)
      );
  }

  /* public handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError(
    'Something bad happened; please try again later.');
  } */
}
