import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';


import { UsersComponent } from './users.component';
import { UserService } from '../services/user.service';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserDeleteComponent } from './user-delete/user-delete.component';

@NgModule({
  declarations: [UsersComponent, UserDetailComponent, UserUpdateComponent, UserCreateComponent, UserDeleteComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ UserService ]
})
export class UsersModule { }
