import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserDeleteComponent } from './user-delete/user-delete.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';
import { UserCreateComponent } from './components/user-create/user-create.component';

const routes: Routes = [
  { path: '', 
    component: UsersComponent ,
    children: [
      {
        path: 'detail/:id',
        component: UserDetailComponent
      },
      {
        path: 'remove/:id',
        component: UserDeleteComponent
      },
      {
        path: 'edit/:id',
        component: UserUpdateComponent
      },
      {
        path: 'create',
        component: UserCreateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
