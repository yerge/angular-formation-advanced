import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user.interface';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  
  detail: User;

  paramSub: Subscription;

  //@Output()
  //remove: EventEmitter<User> = new EventEmitter();

  constructor(private activateRoute: ActivatedRoute,
              private userService: UserService) { 

  }

  ngOnInit(): void {
    // get the user id detail by id
    this.paramSub = this.activateRoute.params.subscribe(data => {
      this.userService.getUserById(data.id)
      .subscribe((data: User) => this.detail = data);
    });
    
  }

  /* onRemove(id) {
    this.userService.deleteUser(id)
      .subscribe();
    //this.remove.emit(this.detail);
  } */

  onEdit(user: User) {
    // to do
  }

  /**
   * ngOnDestroy
 : void  */
  public ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }
}
