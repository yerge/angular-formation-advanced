import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { rangeValidator } from '../../../validators/range-validator.directive';
import { User } from 'src/app/models/user.interface';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {  

  newUser: User;

 // @Output()
  create: EventEmitter<User> = new EventEmitter();

  userForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      name: ['fdg', [Validators.required, Validators.minLength(3), Validators.maxLength(6)]],
      surname: ['dfg', [Validators.required, Validators.minLength(3), Validators.maxLength(6)]],
      age: [55, [Validators.required, rangeValidator(18, 150)]],
      profession: ['dfg', Validators.required],
      address: this.formBuilder.group({
        roadNumber: ['',[Validators.required, rangeValidator(1, 999)]],
        street: ['sdg', Validators.required],
        postalCode: ['1000', [Validators.required, rangeValidator(1000, 99999)]],
        city: ['sdr', Validators.required],
        country: ['sd', Validators.required]
      }),
      mail: ['ffdgfd@ff.com', Validators.email],
      phone: ['']
    });
  }

  onSubmit() {
    console.warn(this.userForm.value);
    this.newUser = new User(this.userForm.value);
    this.userService.createUser(this.newUser)
      .subscribe();
      this.router.navigate(['/users']);
    //this.create.emit(this.newUser);
  }

  get name() {
    return this.userForm.get('name');
  }  

  get age() {
    return this.userForm.get('age');
  }
  
  get surname() {
    return this.userForm.get('surname');
  }

  get profession() {
    return this.userForm.get('profession');
  }

  get mail() {
    return this.userForm.get('mail');
  }

  get address() {
    return this.userForm.controls.address as FormGroup;
  }
}
