import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user.interface';
import { State } from '../models/enum.user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  state: State = State.ITINIAL;
  states = State;
  selectedOneUser: User;
  updateOneUser: User;
  users: User[];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.state = State.ITINIAL;

    // subcribe to creation observer
    this.userService.onUserCreated$()
      .subscribe((user: User) => {
        console.log("clic sur create");
        this.userService.getUsers()
          .subscribe((data: User[]) => {
          this.users = data;
        });
        this.state = State.CREATE;
      });

    // subscribe to update
    this.userService.onUserUpdated$().subscribe((user: User) => {
      console.log("clic sur edit");
      this.updateOneUser = user;
      this.state =  State.ITINIAL;
    });
    
    // subscribe to delete
    this.userService.onUserDeleted$()
        .subscribe((userTodelete: User) => {
          this.users = this.users.filter((user: User) => {
            return user.id !== userTodelete.id;
          });
          this.state = State.DELETE;
          console.log(this.state);
    });

    this.userService.getUsers()
      .subscribe((data: User[]) => {
        this.users = data;
      });
  }


 /*  public onSelect(id: Number): void {
    this.userService.getUserById(id)
          .subscribe((data: User) => {
            this.selectedOneUser = data;
            this.state = State.DETAIL;
          });
  } */

  /* public handleRemove(event: User) {
    this.userService.deleteUser(event.id)
      .subscribe((data: User) => {
        this.users = this.users.filter((user: User) => {
          return user.id !== event.id;
        })
      });
      this.state = State.DELETE;
  } */

  /* public handleCreate(user: User) {
    
    console.log("user.name : " + user.name);
    console.log("user.surname : " + user.surname);
    console.log("user.age : " + user.age);
    console.log("user.profession : " + user.profession);
    console.log("user.mail : " + user.mail);
    console.log("user.phone : " + user.phone);
    console.log("user.address.roadNumber : " + user.address.roadNumber);
    console.log("user.address.street : " + user.address.street);
    console.log("user.address.postalCode : " + user.address.postalCode);
    console.log("user.address.city : " + user.address.city);
    console.log("user.address.country : " + user.address.country);

    // create user
    this.userService.createUser(user)
      .subscribe((data: User) => {
        this.users.push(data);
      });
    this.state =  State.ITINIAL;
  } */

  public handleUpdate(user: User) {
    this.userService.updateUser(user.id, user)
      .subscribe((data: User) => {
        let userIndex = this.users.findIndex(userItem => userItem.id == user.id);
        this.users[userIndex] = user;
      });
    this.state =  State.ITINIAL;
  }

  public get stateEnum() {
    return this.state; 
 }

 public get statesEnum() {
    return this.states; 
  }

  onEdit(user: User) {
    // to do
    console.log("clic sur edit");
    this.updateOneUser = user;
    this.state =  State.UPDATE;
  }

  onCreate() {
    // to do create
    this.state =  State.CREATE;
  }
}
