import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.interface';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit, OnDestroy {

  paramSub: Subscription;
  
  constructor(private activateRoute: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit(): void {
    // Get the id to remove the object
    /* const userId = + this.activateRoute.snapshot.paramMap.get('id');
    this.userService.deleteUser(userId)
      .subscribe(); */

      // intercept all urls
      this.paramSub = this.activateRoute.params
                        .subscribe((userToRemove) => {
                          this.userService.deleteUser(userToRemove.id)
                          .subscribe();
                        });
  }

  public ngOnDestroy(): void {
    this.paramSub.unsubscribe();
  }

}
