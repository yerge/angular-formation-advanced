import { User } from '../models/user.interface';

export class UserUtils {
    public static deepEqualUser(x: User, y: User): boolean {
        if (x === y) {
            return true;
        }

        if ((typeof x == "object" && x != null) && (typeof y == "object" && y != null)) {
            
            return (y.name === x.name) 
                    && (y.surname === x.surname)
                    && (y.age === x.age)
                    && (y.profession === x.profession)
                    && (y.mail === x.mail)
                    && (y.phone === x.phone)
                    && (y.address.roadNumber === x.address.roadNumber)
                    && (y.address.street === x.address.street)
                    && (y.address.postalCode === x.address.postalCode)
                    && (y.address.city === x.address.city)
                    && (y.address.country === x.address.country);
        } else {
            return false;
        }
    }
}