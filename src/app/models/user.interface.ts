import { Address } from './address.interface';
import { JsonProperty } from '../../../node_modules/json-object-mapper';


export class User {

    @JsonProperty({type: Number, name: "id"})
    id: number = void 0;

    @JsonProperty({type: String, name: "name"})
    //@JsonProperty()
    name: string = void 0;

    @JsonProperty({type: String, name: "surname"})
    //@JsonProperty()
    surname: string = void 0;

    @JsonProperty({type: Number, name: "age"})
    //@JsonProperty()
    age: number = void 0;

    @JsonProperty({type: String, name: "profession"})
    //@JsonProperty()
    profession: string = void 0;

    @JsonProperty({type: Address, name: "address"})
    //@JsonProperty()
    address: Address | null = void 0;

    @JsonProperty({type: String, name: "mail"})
    //@JsonProperty()
    mail: string = void 0;

    @JsonProperty({type: String, name: "phone"})
    //@JsonProperty()
    phone: string = void 0;

    public constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

   /*  public deepEqualUser(x: User, y: User): boolean {
        if (x === y) {
            return true;
        }

        if ((typeof x == "object" && x != null) && (typeof y == "object" && y != null)) {
            
            return (y.name === x.name) 
                    && (y.surname === x.surname)
                    && (y.age === x.age)
                    && (y.profession === x.profession)
                    && (y.mail === x.mail)
                    && (y.phone === x.phone)
                    && (y.address.roadNumber === x.address.roadNumber)
                    && (y.address.street === x.address.street)
                    && (y.address.postalCode === x.address.postalCode)
                    && (y.address.city === x.address.city)
                    && (y.address.country === x.address.country);
        } else {
            return false;
        }
    } */

    /* public deepEqualUser(x: User): boolean {
        if (x === this) {
            return true;
        }

        if ((typeof x == "object" && x != null) && (typeof this == "object" && this != null)) {
            
            return (this.name === x.name) 
                    && (this.surname === x.surname)
                    && (this.age === x.age)
                    && (this.profession === x.profession)
                    && (this.mail === x.mail)
                    && (this.phone === x.phone)
                    && (this.address.roadNumber === x.address.roadNumber)
                    && (this.address.street === x.address.street)
                    && (this.address.postalCode === x.address.postalCode)
                    && (this.address.city === x.address.city)
                    && (this.address.country === x.address.country);
        } else {
            return false;
        }
    } */
}