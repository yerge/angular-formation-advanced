import { JsonProperty } from '../../../node_modules/json-object-mapper';

export class Address {

    @JsonProperty({type: Number, name: "roadNumber"})
    //@JsonProperty()
    roadNumber: number = void 0;

    @JsonProperty({type: String, name: "street"})
    //@JsonProperty()
    street: string = void 0;

    @JsonProperty({type: String, name: "postalCode"})
    //@JsonProperty()
    postalCode: number = void 0;

    @JsonProperty({type: String, name: "city"})
    //@JsonProperty()
    city: string = void 0;

    @JsonProperty({type: String, name: "country"})
    //@JsonProperty()
    country: string = void 0;
}